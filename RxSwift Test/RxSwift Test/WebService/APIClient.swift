//
//  APIClient.swift
//  RxSwift Test
//
//  Created by Anargha Jagadeesh on 02/05/21.
//

import Foundation
import RxSwift
import RxCocoa

class APIClient {
  static var shared = APIClient()
  lazy var requestObservable = RequestObservable(config: .default)
  
  func sendPost(requestModel: LoginRequestModel) -> Observable<LoginModel> {
     var request = URLRequest(url:
                                URL(string:requestModel.baseURL)!)
     request.httpMethod = "POST"
     request.addValue("application/json", forHTTPHeaderField:
      "Content-Type")
     let payloadData = try? JSONSerialization.data(withJSONObject:
           requestModel.parameters, options: [])
     request.httpBody = payloadData
     return requestObservable.callAPI(request: request)
   }
}
