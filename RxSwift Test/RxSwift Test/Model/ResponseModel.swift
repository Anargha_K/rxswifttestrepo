//
//  ResponseModel.swift
//  RxSwift Test
//
//  Created by Anargha Jagadeesh on 02/05/21.
//

import Foundation

struct UserModel : Codable {
    var userId : Int
    var userName : String
    var created_at : String
    
    enum CodingKeys : String, CodingKey {
        case userId
        case userName
        case created_at
    }
}

struct UserDataModel : Codable {
    var user : UserModel
    
    enum CodingKeys : String, CodingKey {
        case user
    }
}

struct LoginModel : Codable {
    var result : Int
    var error_message : String
    var data : UserDataModel
    
    enum CodingKeys : String, CodingKey {
        case result
        case error_message
        case data
    }
    
}
