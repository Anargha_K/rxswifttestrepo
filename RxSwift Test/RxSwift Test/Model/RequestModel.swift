//
//  RequestModel.swift
//  RxSwift Test
//
//  Created by Anargha Jagadeesh on 02/05/21.
//

import Foundation

struct LoginRequestModel {
    var baseURL = "http://imaginato.mocklab.io/login"
    var parameters = [String: String]()

    init(email: String, password : String) {
        parameters["email"] = email
        parameters["password"] = password
    }
}
