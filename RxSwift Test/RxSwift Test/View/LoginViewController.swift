//
//  ViewController.swift
//  RxSwift Test
//
//  Created by Anargha Jagadeesh on 02/05/21.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftLoader

class LoginViewController : UIViewController {

    //Outlet Declerations
    
    @IBOutlet weak var viewEmail: CustomTextFieldView!
    @IBOutlet weak var viewPassword: CustomTextFieldView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    var loginData : LoginModel!
    let disposeBag = DisposeBag()
    let viewModel = LoginViewModel()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //Loader config
        self.loaderConfig()
    }

    
    //Outlet Action declerations
    @IBAction func signInPressed(_ sender: UIButton) {
        
        SwiftLoader.show(title: "Loading...", animated: true)
        let client = APIClient.shared
        do{
            client.sendPost(requestModel: LoginRequestModel(email: self.txtEmail.text ?? "", password: self.txtPassword.text ?? "")).subscribe(
                onNext: { result in
                    if result.result != 1 {
                        DispatchQueue.main.async {
                            SwiftLoader.hide()
                            self.showAlert(title: "Error", msg: result.error_message)
                        }
                    } else {
                        self.loginData = result
                        DispatchQueue.main.async {
                            SwiftLoader.hide()
                            self.showAlert(title: "Success", msg: "Login Successful")
                        }
                    }
                    
                },
                onError: { error in
                    DispatchQueue.main.async {
                        SwiftLoader.hide()
                        self.showAlert(title: "Error", msg: "Your email or password is incorrect.")
                    }
                    print(error.localizedDescription)
                },
                onCompleted: {
                    print("Completed event.")
                }).disposed(by: disposeBag)
        }
        
    }
    
    //Loader Config
    func loaderConfig() {
        var config : SwiftLoader.Config = SwiftLoader.Config()
        config.size = 100
        config.spinnerColor = .black
        config.foregroundColor = .white
        config.foregroundAlpha = 0.5
        SwiftLoader.setConfig(config: config)
    }
    
}

