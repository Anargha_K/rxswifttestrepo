//
//  ViewControllerExtensions.swift
//  RxSwift Test
//
//  Created by Anargha Jagadeesh on 03/05/21.
//

import Foundation
import UIKit

extension UIViewController {
    func showAlert(title: String?, msg: String){
        
        let controller = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style:.default, handler: nil)
        controller.addAction(action)
        self.present(controller, animated: true, completion: nil)
    }
}

