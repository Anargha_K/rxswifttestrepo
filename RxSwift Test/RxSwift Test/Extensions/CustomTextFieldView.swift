//
//  CustomTextFieldView.swift
//  RxSwift Test
//
//  Created by Anargha Jagadeesh on 02/05/21.
//

import Foundation
import UIKit

class CustomTextFieldView: UIView {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 1
        self.layer.borderColor = #colorLiteral(red: 0.3254901961, green: 0.6862745098, blue: 0.7529411765, alpha: 1)
        self.backgroundColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2588235294, alpha: 1)
    }
}
