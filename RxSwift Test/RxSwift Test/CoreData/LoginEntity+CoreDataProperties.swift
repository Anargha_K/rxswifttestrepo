//
//  LoginEntity+CoreDataProperties.swift
//  
//
//  Created by Anargha Jagadeesh on 02/05/21.
//
//

import Foundation
import CoreData


extension LoginEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<LoginEntity> {
        return NSFetchRequest<LoginEntity>(entityName: "LoginEntity")
    }

    @NSManaged public var userId: Int16
    @NSManaged public var userName: String?
    @NSManaged public var created_at: Date?

}
