//
//  LoginViewModel.swift
//  RxSwift Test
//
//  Created by Anargha Jagadeesh on 02/05/21.
//

import UIKit
import CoreData



class LoginViewModel: NSObject {
    func saveToCoreData(loginModel : LoginModel){
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        let loginDict = LoginEntity(context: managedContext)
        loginDict.userId = Int16(loginModel.data.user.userId)
        loginDict.userName = loginModel.data.user.userName
        loginDict.created_at = self.convertStringToDate(dateStr: loginModel.data.user.created_at)
        
        do {
            try managedContext.save()
            //          basicFirst.append(basicFirstVal)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func convertStringToDate(dateStr : String) -> Date {
        var convertedDate = Date()
        if let date = dateStr.iso8601withFractionalSeconds {
            date.description(with: .current)
            convertedDate = date
        }
        return convertedDate
    }
    
    
    //Loader Configuration
    func loaderCongif() {
        
    }

}
